# Documentation of VSCode Text Edit Manager

> Set's your coding experience with VSCode text editing into the another level!

This package simplifies to workflow of editing documents with the API of VSCode.

Pleas see the [documentation](https://gitlab.com/vscode1/text-edit-manager/-/wikis/documentation) for more information.
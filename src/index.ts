import { TextEdit, TextLine, Position, Range, TextDocument, TextEditorEdit, TextEditor } from 'vscode'
import { unset, map, values } from 'lodash'

export class TextEditManager {
  private documentLineEdits: Map<string | number, TextEditManagerEdit>
  private static activeManager: TextEditManager
  private editor: TextEditor
  private insertCounter: number

  constructor(editor?: TextEditor) {
    this.editor = editor
    this.documentLineEdits = new Map()
    this.insertCounter = 0
    TextEditManager.activeManager = this
  }

  addLineEdit(edit: TextEditManagerEdit) {
    if (edit.getDescription() === 'insert') {
      if (this.lineIsDeleted(edit.getLineNumber())) {
        let msg = 'Line will be deleted. Insert operation is not set'
        msg += '/n operation ' + edit.getDescription() + ' at rowIndex ' + edit.getLineNumber()
        console.error(msg)
        return
      } else if (!this.lineIsOverwriteable(edit.getLineNumber())) {
        let msg = 'Line is not overwriteable. Operation is not set'
        msg += '/n operation ' + edit.getDescription() + ' at rowIndex ' + edit.getLineNumber()
        console.error(msg)
        return
      } else {
        this.documentLineEdits.set('i' + this.insertCounter++, edit)
        return
      }
    } else {
      if (!this.lineIsOverwriteable(edit.getLineNumber())) {
        let msg = 'Line is not overwriteable. Operation is not set'
        msg += '/n operation ' + edit.getDescription() + ' at rowIndex ' + edit.getLineNumber()
        console.error(msg)
        return
      }
      this.documentLineEdits.set(edit.getLineNumber(), edit)
    }
  }

  static addLineEdit(lineEdit: TextEditManagerEdit) {
    TextEditManager.getActiveManager().addLineEdit(lineEdit)
  }

  lineIsDeleted(index: number) {
    if (this.documentLineEdits.has(index)) {
      let item = this.documentLineEdits.get(index)
      if (item.isImplemented('TextEditManagerLineEdit')) {
        let lineEdit = item as TextEditManagerLineEdit

        if (lineEdit.getDescription() === 'delete') {
          return true
        }
      }
    }

    return false
  }

  static lineIsDeleted(index: number) {
    this.activeManager.lineIsDeleted(index)
  }

  lineIsOverwriteable(index: number) {
    if (!this.documentLineEdits.has(index)) {
      return true
    }
    let item = this.documentLineEdits.get(index)
    if (item.isImplemented('TextEditManagerLineEdit')) {
      let lineEdit = item as TextEditManagerLineEdit
      return lineEdit.isOverwriteable()
    }

    return true
  }

  static lineIsOverwriteable(index: number) {
    this.activeManager.lineIsOverwriteable(index)
  }

  getTextEdits(): TextEdit[] {
    return Array.from(this.documentLineEdits.values()).map((items) => {
      return items.getTextEdit()
    })
  }

  setTextEdits(editBuilder: TextEditorEdit): void {
    this.documentLineEdits.forEach((value, key) => {
      value.setTextEdit(editBuilder)
    })
  }

  runEdits(): void {
    if (this.getEditor()) {
      this.getEditor().edit((editBuilder: TextEditorEdit) => {
        this.setTextEdits(editBuilder)
      })
    }
  }

  lineAt(index: number) {
    if (this.getEditor()) {
      return DocumentLine.lineAt(this.editor.document, index)
    }
    return null
  }

  *makeLineIterator(start = 0, end: number = null, step = 1) {
    if (end === null) {
      if (this.getEditor()) {
        this.getEditor().document.lineCount
        return null
      }
    }
    let iterationCount = 0
    for (let i = start; i < end; i += step) {
      iterationCount++
      yield this.lineAt(i)
    }
    return iterationCount
  }

  static *makeLineIterator(document: TextDocument, start = 0, end = document.lineCount, step = 1) {
    let iterationCount = 0
    for (let i = start; i < end; i += step) {
      iterationCount++
      yield DocumentLine.lineAt(document, i)
    }
    return iterationCount
  }

  static getActiveManager() {
    return TextEditManager.activeManager
  }

  getEditor() {
    if (this.editor) {
      return this.editor
    } else {
      throw new ReferenceError('TextEditManager: Variable editor not set')
    }
  }

  setEditor(editor: TextEditor) {
    this.editor = editor
  }
}

export class DocumentLine {
  private static lines: { [key: string]: DocumentLine } = {}
  private lineNumber: number
  private text: string
  private rangeIncludingLineBreak: Range
  private range: Range
  private firstNonWhitespaceCharacterIndex: number

  constructor(
    lineNumber: number,
    text: string,
    range: Range = null,
    rangeIncludingLineBreak: Range = null,
    firstNonWhitespaceCharacterIndex: number = -1
  ) {
    this.lineNumber = lineNumber
    this.text = text
    this.range = range
    this.rangeIncludingLineBreak = rangeIncludingLineBreak
    this.firstNonWhitespaceCharacterIndex = firstNonWhitespaceCharacterIndex

    DocumentLine.lines[lineNumber] = this
  }

  getLineNumber() {
    return this.lineNumber
  }
  getText() {
    return this.text
  }
  setText(text: string) {
    this.text = text
  }
  getRangeIncludingLineBreak() {
    return this.rangeIncludingLineBreak
  }
  getRange() {
    return this.range
  }
  getFirstNonWhitespaceCharacterIndex() {
    return this.firstNonWhitespaceCharacterIndex
  }

  static lineAt(document: TextDocument, index: number) {
    return DocumentLine.fromLine(document.lineAt(index))
  }

  static fromLine(line: TextLine) {
    return new DocumentLine(
      line.lineNumber,
      line.text,
      line.range,
      line.rangeIncludingLineBreak,
      line.firstNonWhitespaceCharacterIndex
    )
  }
}

export class DocumentLineGroup {
  static groups: { [key: string]: DocumentLineGroup } = {}
  static idCounter: number = 0
  lines: { [key: string]: DocumentLine }
  id: number
  active: Boolean
  name: string

  constructor(name: string = null) {
    this.lines = {}
    this.id = DocumentLineGroup.idCounter++
    this.active = true
    this.name = name

    if (name) {
      DocumentLineGroup.groups[name] = this
    }
  }

  addLine(line: DocumentLine) {
    if (this.active) {
      this.lines[line.getLineNumber()] = line
    }
  }

  getLines() {
    return values(this.lines)
  }

  setLines(lineArray: DocumentLine[]) {
    this.lines = {}
    lineArray.forEach((line) => {
      this.addLine(line)
    })
  }

  removeLine(lineNumber: number) {
    unset(this.lines, lineNumber)
  }

  static getGroup(name: string): DocumentLineGroup {
    return this.groups[name]
  }

  static removeGroup(name: string) {
    unset(this.groups, name)
  }

  isActive() {
    return this.active
  }

  setActive() {
    this.active = true
  }

  isInactive() {
    return !this.isActive()
  }

  setInactive() {
    this.active = false
  }
}

interface TextEditManagerEdit {
  isImplemented: (interfaceName: string) => boolean
  getDescription: () => string
  getTextEdit: () => TextEdit
  setTextEdit: (editBuilder: TextEditorEdit) => void
  getLineNumber: () => number
}

interface TextEditManagerLineEdit extends TextEditManagerEdit {
  getLine: () => DocumentLine
  isOverwriteable: () => boolean
  setOverwriteable: (overwriteable: boolean) => void
}

export class TextEditLineDelete implements TextEditManagerLineEdit {
  private implements: string[] = ['TextEditManagerEdit', 'TextEditManagerLineEdit']
  private overwriteable: boolean
  private description: string
  private line: DocumentLine

  constructor(line: DocumentLine, overwriteable = false) {
    this.description = 'delete'
    this.line = line
    this.overwriteable = overwriteable
  }

  getTextEdit() {
    return TextEdit.delete(this.line.getRangeIncludingLineBreak())
  }

  setTextEdit(editBuilder: TextEditorEdit) {
    editBuilder.delete(this.line.getRangeIncludingLineBreak())
  }

  getLineNumber() {
    return this.line.getLineNumber()
  }

  getLine() {
    return this.line
  }

  getDescription() {
    return this.description
  }

  isOverwriteable() {
    return this.overwriteable
  }

  setOverwriteable(overwriteable: boolean) {
    this.overwriteable = overwriteable
  }

  isImplemented(interfaceName: string) {
    return this.implements.includes(interfaceName)
  }
}

export class TextEditLineReplace implements TextEditManagerLineEdit, TextEditManagerEdit {
  private implements: string[] = ['TextEditManagerEdit', 'TextEditManagerLineEdit']
  private description: string
  private overwriteable: boolean
  private line: DocumentLine

  constructor(line: DocumentLine, overwriteable = true) {
    this.description = 'replace'
    this.line = line
    this.overwriteable = overwriteable
  }

  static removeStartingSpaces(line: DocumentLine): TextEditLineReplace {
    line.setText(line.getText().trim())
    return new TextEditLineReplace(line)
  }

  getTextEdit() {
    return TextEdit.replace(this.line.getRange(), this.line.getText())
  }

  setTextEdit(editBuilder: TextEditorEdit) {
    return editBuilder.replace(this.line.getRange(), this.line.getText())
  }

  getLineNumber(): number {
    return this.line.getLineNumber()
  }

  getLine() {
    return this.line
  }

  getDescription() {
    return this.description
  }

  isOverwriteable() {
    return this.overwriteable
  }

  setOverwriteable(overwriteable: boolean) {
    this.overwriteable = overwriteable
  }

  isImplemented(interfaceName: string) {
    return this.implements.includes(interfaceName)
  }
}

export class TextEditInsert implements TextEditManagerEdit {
  private implements: string[] = ['TextEditManagerEdit']
  private description: string
  private text: string
  private pos: Position

  constructor(pos: Position, text: string) {
    this.description = 'insert'
    this.text = text
    this.pos = pos
  }

  isImplemented(interfaceName: string) {
    return this.implements.includes(interfaceName)
  }

  static fromPrimitives(lineNumber: number, linePos: number, text: string) {
    return new TextEditInsert(new Position(lineNumber, linePos), text)
  }

  getTextEdit() {
    return TextEdit.insert(this.pos, this.text)
  }

  setTextEdit(editBuilder: TextEditorEdit) {
    return editBuilder.insert(this.pos, this.text)
  }

  getLineNumber() {
    return this.pos.line
  }

  getText() {
    return this.text
  }

  setText(text: string) {
    this.text = text
  }

  getPosition() {
    return this.pos
  }

  setPosition(pos: Position) {
    this.pos = pos
  }

  getDescription() {
    return this.description
  }
}
